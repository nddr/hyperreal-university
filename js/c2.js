document.addEventListener('DOMContentLoaded', function () {
  const input = document.querySelector('input');
  const button = document.querySelector('.button');
  const tasksEl = document.querySelector('.tasks');

  button.addEventListener('click', function () {
    const taskEl = document.createElement('div');
    taskEl.innerHTML = input.value;
    taskEl.classList.add('task');
    tasksEl.appendChild(taskEl);
  });
});
