document.addEventListener('DOMContentLoaded', async () => {
  const name = document.querySelector('#name');
  const email = document.querySelector('#email');
  const message = document.querySelector('#message');
  const response = document.querySelector('.response');
  const form = document.querySelector('form');

  form.addEventListener('submit', (e) => {
    e.preventDefault();

    const data = {
      name: name.value,
      email: email.value,
      message: message.value,
    };

    fetch('https://hyperreal-university.netlify.app/.netlify/functions/email', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        cors: 'no-cors',
      },
      body: JSON.stringify(data),
    }).then((res) => {
      console.log('res', res);
      response.style.display = 'flex';
      if (res.ok) response.textContent = 'Message sent successfully';
      else response.textContent = 'Message not sent';
    });
  });
});