document.addEventListener('DOMContentLoaded', async () => {
  const cards = document.querySelector('.carousel-cards');
  const prevBtn = document.querySelector('.prev');
  const nextBtn = document.querySelector('.next');
  const totalPhotos = 4;
  let currentPhoto = 1;

  prevBtn.addEventListener('click', () => {
    if (currentPhoto > 1) {
      cards.style.transform += `translateX(450px)`;
      currentPhoto -= 1;
    }
  });

  nextBtn.addEventListener('click', () => {
    if (currentPhoto < totalPhotos) {
      cards.style.transform += `translateX(-450px)`;
      currentPhoto += 1;
    }
  });
});