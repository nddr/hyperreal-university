document.addEventListener('DOMContentLoaded', async () => {
  const cartItems = document.querySelector('.cart-items');
  const totalEl = document.querySelector('.cart-details');
  const items = document.querySelectorAll('.item');

  // check localstorage for cart items
  let cart = JSON.parse(localStorage.getItem('cart')) ?? [];

  items.forEach(item => {
    item.addEventListener('click', (e) => {
      addToCart({
        id: e.target.dataset.id,
        name: e.target.dataset.name,
        price: e.target.dataset.price
      });
    });
  });

  cartItems.addEventListener('click', (e) => {
    // remove clicked item from the array
    removeFromCart(e.target.dataset.key);
  });

  const addToCart = (item) => {
    cart.push(item);

    // set cart into localstorage
    localStorage.setItem('cart', JSON.stringify(cart));

    renderCart();
  }

  const removeFromCart = (index) => {
    cart.splice(index, 1);

    // set cart into localstorage
    localStorage.setItem('cart', JSON.stringify(cart));

    renderCart();
  }

  const renderCart = () => {
    // update total
    let total = 0;

    // clear cart, then render
    cartItems.innerHTML = '';
    cart.forEach((item, idx) => {
      const newItem = document.createElement('div');
      newItem.classList.add('cart-item');
      newItem.dataset.id = item.id;
      newItem.dataset.key = idx;
      newItem.innerHTML = `
        <span>${item.name}</span> <span class="cart-item--price">$${item.price}</span>
      `;
      cartItems.appendChild(newItem);
      total += parseInt(item.price);
    });

    totalEl.textContent = `Total: $${total}`;
  }

  renderCart();
});