// Since we are working with the DOM, let's wait until the DOM is ready
document.addEventListener('DOMContentLoaded', function () {
  console.log('DOM loaded');

  // Code goes here
});