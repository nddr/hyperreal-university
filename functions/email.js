exports.handler = async (event) => {
  const { email, name, message } = JSON.parse(event.body);

  console.log(email, name, message);

  if (email && name && message) return { statusCode: 200 };
  else return { statusCode: 500 };
};
